<?php

namespace TableOutput\Render;

use TableOutput\Column\Colspan;
use TableOutput\Row\ARow;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSummary;
use TableOutput\Table;

class RenderTXT implements IRender
{
    protected $result = '';
    /**
     * @var \stdClass
     */
    protected $renderAsObject;
    protected $resultInherited = false;

    public function __construct(Table $table, $renderAsObject = null)
    {
        if($renderAsObject instanceof \stdClass){
            $this->renderAsObject = $renderAsObject;
            $this->resultInherited = true;
        }else{
            $this->renderAsObject = new \stdClass();
            $this->renderAsObject->result = '';
        }
        $footer = $table->getSummaryRow();
        if(isset($footer)) $footer->beforeRender();
    }

    public function tableCaption($caption, $mergedCols)
    {
        $this->renderAsObject->result .= $caption.PHP_EOL;
    }

    public function tableHeader(RowHeader $row)
    {
        $this->renderTableRow($row);
        return $this;
    }

    protected function renderTableRow(ARow $row){
        foreach($row->getColumns() as $column){
            if($column instanceof Colspan) continue;
            $column->beforeRender();
            $this->renderAsObject->result .= str_pad($column->value, $column->width);
        }
        $this->renderAsObject->result .= PHP_EOL;
    }

    public function tableRow(ARow $row)
    {
        $this->renderTableRow($row);
        return $this;
    }

    public function tableFooter(RowSummary $row = null, $render = true)
    {
        if(isset($row)) $this->renderTableRow($row);
        return $this;
    }

    public function render()
    {
        if(!$this->resultInherited) return $this->renderAsObject->result;
    }
}
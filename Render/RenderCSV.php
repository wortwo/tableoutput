<?php

namespace TableOutput\Render;

use TableOutput\Column\Colspan;
use TableOutput\Row\ARow;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSummary;
use TableOutput\Table;

/**
 * Class RenderCSV
 */
class RenderCSV implements IRender
{
    protected $result = '';

    /**
    * @var \stdClass
    */
    protected $renderAsObject;
    protected $resultInherited = false;

    /**
     * @param Table $table
     */
    public function __construct(Table $table, $renderAsObject = null)
    {
        if($renderAsObject instanceof \stdClass){
            $this->renderAsObject = $renderAsObject;
            $this->resultInherited = true;
        }else{
            $this->renderAsObject = new \stdClass();
            $this->renderAsObject->result = '';
        }
        $footer = $table->getSummaryRow();
        if(isset($footer)) $footer->beforeRender();
    }

    /**
     * @param $caption
     * @param $mergedCols
     * @return $this
     */
    public function tableCaption($caption, $mergedCols)
    {
        //$this->renderAsObject->result .= $caption.PHP_EOL;
        return $this;
    }

    /**
     * @param RowHeader $row
     * @return $this
     */
    public function tableHeader(RowHeader $row)
    {
        $this->renderTableRow($row);
        return $this;
    }

    /**
     * Render row
     * @param ARow $row
     */
    protected function renderTableRow(ARow $row){
        $result = [];
        foreach($row->getColumns() as $column){
            if($column instanceof Colspan){
                $result[] = '';
                continue;
            }
            $column->beforeRender();
            $result[] = $column->value;
        }
        ob_start();
        $out = fopen('php://output', 'w');
        fputcsv($out, $result);
        fclose($out);
        $this->renderAsObject->result .= ob_get_clean();
    }

    /**
     * @param ARow $row
     * @return $this
     */
    public function tableRow(ARow $row)
    {
        $this->renderTableRow($row);
        return $this;
    }

    /**
     * @param RowSummary $row
     * @return $this
     */
    public function tableFooter(RowSummary $row = null, $render = true)
    {
        //$this->renderTableRow($row);
        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        if(!$this->resultInherited) return $this->renderAsObject->result;
    }
}
<?php

namespace TableOutput\Render;

use TableOutput\Column\Colspan;
use TableOutput\Row\ARow;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSummary;
use TableOutput\Table;

/**
 * Class RenderLATTE
 * @package Table\Render
 */
class RenderLATTE implements IRender
{
    protected $result = '';
    /**
     * @var \stdClass
     */
    protected $renderAsObject;
    protected $resultInherited = false;
    protected $table;

    public function __construct(Table $table, $renderAsObject = null)
    {
        if($renderAsObject instanceof \stdClass){
            $this->renderAsObject = $renderAsObject;
            $this->resultInherited = true;
        }else{
            $this->renderAsObject = new \stdClass();
            $this->renderAsObject->result = '<div class="card mb-3"><table class="table table-hover table-sm mb-0">';
        }
        $footer = $table->getSummaryRow();
        $this->table = $table;
        if(isset($footer)) $footer->beforeRender();
    }

    public function tableCaption($caption, $mergedCols)
    {
        $this->renderAsObject->result .= '<caption class="card-header h6" style="caption-side: top;"><form class="row form-inline">
            <div class="col-10">'.$caption.'</div>
            <div class="input-group col-2">
                <select name="exportTableType" class="form-control form-control-sm border-right-0" aria-label="Recipient\'s username" aria-describedby="basic-addon2">
                <option value="" disabled selected>Export</option>';
        foreach ($this->table->getAvailableRenderTypes() as $type) {
            $this->renderAsObject->result .= '<option value="'.$type.'">'.$type.'</option>';
        }
        $this->renderAsObject->result .= '</select>
                <button type="submit" class="input-group-addon float-right p-1 exportTableSubmit btn btn-primary" id="basic-addon2"><i href="" class="fa fa-download"></i></button>
            </div>
            </form></caption>';
    }

    public function tableHeader(RowHeader $row)
    {
        $this->renderTableHeaderRow($row);
        return $this;
    }

    protected function renderTableHeaderRow(ARow $row){
        $this->renderAsObject->result .= '<thead class="thead-default"><tr>';
        foreach($row->getColumns() as $column){
            if($column instanceof Colspan) continue;
            $column->beforeRender();
            $this->renderAsObject->result .= '<th>'.str_pad($column->value, $column->width).'</th>';
        }
        $this->renderAsObject->result .= '</tr></thead><tbody class="">';
    }

    protected function renderTableRow(ARow $row){
        $this->renderAsObject->result .= '<tr>';
        foreach($row->getColumns() as $column){
            if($column instanceof Colspan) continue;
            if(!empty($column->value)) $column->beforeRender();
            $this->renderAsObject->result .= '<td>'.str_pad($column->value, $column->width).'</td>';
        }
        $this->renderAsObject->result .= '</tr>';
    }

    public function tableRow(ARow $row)
    {
        $this->renderTableRow($row);
        return $this;
    }

    public function tableFooter(RowSummary $row = null, $render = true)
    {
        if(isset($row)) $this->renderTableRow($row);
        return $this;
    }

    public function render()
    {
        $this->renderAsObject->result .= '</tbody></table></div>';
        if(!$this->resultInherited) return $this->renderAsObject->result;
    }
}
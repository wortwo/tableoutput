<?php

namespace TableOutput\Render;

interface IProgress
{
    public function addStep();
    public function progressDone();
}
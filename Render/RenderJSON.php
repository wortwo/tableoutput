<?php

namespace TableOutput\Render;

use TableOutput\Column\Colspan;
use TableOutput\Row\ARow;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSummary;
use TableOutput\Table;

class RenderJSON implements IRender
{
    /**
     * @var \stdClass
     */
    protected $renderAsObject;
    protected $resultInherited = false;

    public function __construct(Table $table, $renderAsObject = null)
    {
        if($renderAsObject instanceof \stdClass){
            $this->renderAsObject = $renderAsObject;
            $this->resultInherited = true;
        }else{
            $this->renderAsObject = new \stdClass();
            $this->renderAsObject->result = [];
        }
        $footer = $table->getSummaryRow();
        if(isset($footer)) $footer->beforeRender();
    }

    public function tableCaption($caption, $mergedCols)
    {
        $this->renderAsObject->result['caption'] = $caption;
    }

    public function tableHeader(RowHeader $row)
    {
        $this->renderAsObject->result['header'] = $this->renderTableRow($row);
        return $this;
    }

    protected function renderTableRow(ARow $row){
        $result = [];
        foreach($row->getColumns() as $column){
            if($column instanceof Colspan){
                $result[] = '';
                continue;
            }
            $column->beforeRender();
            $result[] = $column->value;
        }
        return $result;
    }

    public function tableRow(ARow $row)
    {
        $this->renderAsObject->result['rows'][] = $this->renderTableRow($row);
        return $this;
    }

    public function tableFooter(RowSummary $row = null, $render = true)
    {
        if(isset($row)) $this->renderAsObject->result['footer'] = $this->renderTableRow($row);
        return $this;
    }

    public function render()
    {
        if(!$this->resultInherited) return json_encode($this->renderAsObject->result);
    }
}
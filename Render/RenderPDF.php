<?php

namespace TableOutput\Render;

if(! @include_once('tcpdf.php')) throw new \Exception('tcpdf.php does not exist');


use TableOutput\Column\Colspan;
use TableOutput\Row\ARow;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSummary;
use TableOutput\Table;

class RenderPDF extends TCPDF implements IRender
{
    CONST TYPE = 'PDF';

    public $percentWidth;
    CONST CELL_HEIGHT = 6;
    CONST PAGE_HEIGHT = 258;

    protected $maxItemsOnPage;
    protected $itemsOnPage = 0;

    protected $header;

    /**
     * @var TCPDF
     */
    protected $object;

    protected static $align = [
        'left' => 'L',
        'right' => 'R',
        'center' => 'C'
    ];

    public function __construct(Table $table, $renderAsObject = null){
        parent::__construct();
        $this->maxItemsOnPage = floor(self::PAGE_HEIGHT/self::CELL_HEIGHT);
        $footer = $table->getSummaryRow();
        $this->header = $table->getHeaderRow();
        if(isset($footer)) $footer->beforeRender();
        if($renderAsObject instanceof pdf) {
            $this->object = $renderAsObject;
            $this->setUp($renderAsObject->getMarginlessPageWidth());
        }else{
            $this->object = $this;
            $this->setUp(190);
            $this->object->AddPage();
            $this->object->SetFont('helvetica', '', 10);
        }
    }

    protected function setUp($pageWidth){
        $this->object->SetDrawColor(220,220,220);
        $this->object->SetFillColor(232, 232, 232);
        $this->object->SetTextColor(0);
        $this->object->SetLineWidth(0.2);
        $this->percentWidth = round($pageWidth / 100,2);
    }

    public function tableCaption($caption,$mergedCols)
    {
        if(empty($caption)) return $this;
        $this->object->SetFont('dejavusans', 'B', 10);
        $this->object->Cell($mergedCols*10,6,$caption);
        $this->object->Ln();
        return $this;
    }

    public function tableHeader(RowHeader $row)
    {
        $this->renderTableRow($row);
        return $this;
    }

    protected function renderTableRow(ARow $row){
        foreach ($row->getColumns() as $column){
            if($column instanceof Colspan) continue;
            $column->beforeRender();
            $this->object->SetFont('dejavusans', ($column->bold ? 'B':''), $column->fontSize);
            $this->object->Cell(round($this->percentWidth*$column->width,2),$column->height,$column->value,$column->border,false,self::$align[$column->align],$column->background);
        }
        $this->object->Ln();
    }

    public function tableRow(ARow $row)
    {
        $this->itemsOnPage++;
        if($this->itemsOnPage > $this->maxItemsOnPage){
            $this->itemsOnPage = 0;
            if(is_callable([$this->object,'addPageWithInfoBlock'])){
                $this->object->addPageWithInfoBlock();
            }else{
                $this->object->AddPage();
            }
            $this->tableHeader($this->header);
        }
        $this->renderTableRow($row);
        return $this;
    }

    public function tableFooter(RowSummary $row = null, $render = true)
    {
        if(isset($row)) $this->renderTableRow($row);
        return $this;
    }

    public function render()
    {
        if($this->object instanceof RenderPDF) return @$this->Output(null, 'S');
    }
}
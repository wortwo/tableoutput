<?php

namespace TableOutput\Render;

if(! @include_once('PHPExcel/Classes/PHPExcel.php')) throw new \Exception('PHPExcel/Classes/PHPExcel.php does not exist');

use TableOutput\Column\Colspan;
use TableOutput\Column\Column;
use TableOutput\Column\Header;
use TableOutput\Column\Rowspan;
use TableOutput\Row\ARow;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSummary;
use TableOutput\Table;

class RenderXLS extends PHPExcel implements IRender
{
    CONST TYPE = 'XLS';

    protected $outputFormats = [
        Column::OUTPUT_MONEY => '# ##0.00\ \K\č',
        Column::OUTPUT_FLOAT_M => '# ##0.00\ \m',
        Column::OUTPUT_FLOAT_M2 => '# ##0.00\ \m\²',
        Column::OUTPUT_FLOAT_3 => '# ##0.000',
        Column::OUTPUT_FLOAT_2 => '# ##0.00',
        Column::OUTPUT_FLOAT_6 => '# ##0.000000',
        Column::OUTPUT_PERCENT => '#\ \%',
        Column::OUTPUT_NUMBER => '# ###',
        Column::OUTPUT_DATETIME => 'hh:mm dd.mm.yyyy',
        Column::OUTPUT_DATE => 'dd.mm.yyyy',
        Column::OUTPUT_MONTH_YEAR => 'mm\/yyyy',
        Column::OUTPUT_STR_LENGTH => '@'
    ];

    protected $columnMinWidths = [];

    /**
     * @var PHPExcel_Worksheet
     */
    protected $list;
    protected $workSheetInherited = false;
    protected $actualRowNumber = 1;
    protected $autosize = null;
    protected $recalculateWidths = true;

    public function __construct(Table $table, $renderAsObject = null){
        parent::__construct();
        foreach ($this->outputFormats as $key => $value) {
            $this->outputFormats[$key] = $value;
        }

        if($renderAsObject instanceof PHPExcel_Worksheet) {
            $this->list = $renderAsObject;
            $this->actualRowNumber = $this->list->getHighestRow();
            if($this->actualRowNumber > 1) $this->actualRowNumber +=2;
            $this->workSheetInherited = true;
        }else{
            $this->setUp();
        }
    }

    protected function setUp(){
        $this->setActiveSheetIndex(0);
        $this->list = $this->getActiveSheet();
    }

    public function tableCaption($caption,$mergedCols)
    {
        if(empty($caption)) return $this;
        $this->list->mergeCells('A'.$this->actualRowNumber.':'.$this->getNameFromNumber($mergedCols).$this->actualRowNumber);
        $cell = $this->list->getCell('A'.$this->actualRowNumber);
        $cell->setValueExplicit($caption);
        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 10,
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_HAIR,
                ],
            ],
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_NONE,
            ],
        ];
        $cell->getStyle()->applyFromArray($styleArray);
        $this->actualRowNumber++;
        return $this;
    }

    public function tableHeader(RowHeader $row)
    {
        if(!isset($this->autosize)) $this->autosize = (boolean)(PHPExcel_Shared_Font::getAutoSizeMethod() == PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
        PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_APPROX);
        $rowKeys = array_keys($row->getColumns());
        /** @var Column $column */
        foreach (array_values($row->getColumns()) as $key => $column) {
            $column->outputFormat = Column::OUTPUT_DEFAULT;
            if(is_null($column->value)){
                $row->deleteColumn($rowKeys[$key]);
                continue;
            }
            if(!$column instanceof Header) continue;
            if($this->autosize){
                $this->list->getColumnDimension($this->getNameFromNumber($key+1))->setAutoSize(true);
            }else{
                $this->list->getColumnDimension($this->getNameFromNumber($key+1))->setWidth($column->width);
            }
        }
        $this->renderTableRow($row);
        return $this;
    }

    //TODO: public $height;                ->getRowDimension('10')->setRowHeight(100);
    protected function renderTableRow(ARow $row){
        $previousCellNumber = NULL;
        /** @var Column $column */
        foreach (array_values($row->getColumns()) as $key => $column) {
            if($column instanceof Colspan && isset($previousCellNumber)){
                $this->list->mergeCellsByColumnAndRow($previousCellNumber,$this->actualRowNumber,$key,$this->actualRowNumber);
                continue;
            }
            if($column instanceof Rowspan){
                $this->recalculateWidths = true;
                $this->list->mergeCellsByColumnAndRow($key,$this->actualRowNumber-1,$key,$this->actualRowNumber);
                continue;
            }
            $previousCellNumber = $key;
            if(! $row instanceof RowSummary && !array_key_exists($column->outputFormat,$this->outputFormats)) $column->beforeRender();
            $cell = $this->list->getCellByColumnAndRow($key,$this->actualRowNumber);
            if(!empty($column->value) && in_array($column->outputFormat,[Column::OUTPUT_DATE,Column::OUTPUT_DATETIME])) {
                $column->value = 25569 + (strtotime($column->value) / 86400);
            }


            if($column->outputFormat == Column::OUTPUT_FORMULA && preg_match_all('/\$\[([a-z]+)\]\$/i',$column->value,$matches)){
                foreach ($matches[1] as $position => $colName) {
                    $value = $this->getNameFromNumber($row->getColumnIndex($colName)+1).$this->actualRowNumber;
                    $column->value = str_replace($matches[0][$position],$value,$column->value);
                }
                $column->dataType = PHPExcel_Cell_DataType::TYPE_FORMULA;
                $column->value = '='.$column->value;
            }
            if($column->dataType != PHPExcel_Cell_DataType::TYPE_FORMULA && (!isset($this->columnMinWidths[$this->getNameFromNumber($key+1)]) || $this->columnMinWidths[$this->getNameFromNumber($key+1)] < mb_strwidth($column->value))) $this->columnMinWidths[$this->getNameFromNumber($key+1)] = mb_strwidth($column->value);

            if(isset($column->dataType)){
                $cell->setValueExplicit($column->value,$column->dataType);
            }else{
                $cell->setValue($column->value);
            }
            $borderStyle = [];
            if($column->border === Column::BORDER_TOP_BOTTOM){
                $borderStyle['top'] = ['style' =>PHPExcel_Style_Border::BORDER_HAIR];
                $borderStyle['bottom'] = ['style' =>PHPExcel_Style_Border::BORDER_HAIR];
            }elseif($column->border === Column::BORDER_LEFT_RIGHT){
                $borderStyle['left'] = ['style' =>PHPExcel_Style_Border::BORDER_HAIR];
                $borderStyle['right'] = ['style' =>PHPExcel_Style_Border::BORDER_HAIR];
            }elseif($column->border === false){
                $borderStyle['allborders'] = ['style' =>PHPExcel_Style_Border::BORDER_NONE];
            }else{
                $borderStyle['allborders'] = ['style' =>PHPExcel_Style_Border::BORDER_HAIR];
            }
            $styleArray = [
                'font' => [
                    'bold' => $column->bold,
                    'size' => $column->fontSize
                ],
                'alignment' => [
                    'horizontal' => $column->align,
                ],
                'borders' => $borderStyle,
                'fill' => [
                    'type' => ($column->background) ? PHPExcel_Style_Fill::FILL_SOLID:PHPExcel_Style_Fill::FILL_NONE,
                    'startcolor' => [
                        'argb' => 'FFE7E7E7',
                    ]
                ],
                'numberformat' => [
                    'code' => (array_key_exists($column->outputFormat,$this->outputFormats)) ? $this->outputFormats[$column->outputFormat]:'@'
                ],
            ];
            $this->list->getStyle($this->getNameFromNumber($key+1).$this->actualRowNumber)->applyFromArray($styleArray,false);
        }
        $this->actualRowNumber++;
    }

    public function tableRow(ARow $row)
    {
        $this->renderTableRow($row);
        return $this;
    }

    public function tableFooter(RowSummary $row = null, $render = true)
    {
        if(!isset($row)) return $this;
        /** @var Column $column */
        foreach (array_values($row->getColumns()) as $key => $column) {
            $columnName = $this->getNameFromNumber($key+1);
            if($column->value == RowSummary::SUMA){
                $column->dataType = PHPExcel_Cell_DataType::TYPE_FORMULA;
                $column->value = '=SUM('.$columnName.'3:'.$columnName.($this->actualRowNumber-1).')';
            }elseif($column->value == RowSummary::COUNT){
                $column->dataType = PHPExcel_Cell_DataType::TYPE_FORMULA;
                $column->value = '=COUNTA('.$columnName.'3:'.$columnName.($this->actualRowNumber-1).')';
            }
        }
        if($render) $this->renderTableRow($row);
        return $this;
    }

    public function render()
    {
        if($this->recalculateWidths) $this->recalculateColumnWidths();
        if($this->workSheetInherited) return;
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel5');
        ob_start();
        @$objWriter->save('php://output');
        return ob_get_clean();
    }

    protected function recalculateColumnWidths(){
        $this->list->calculateColumnWidths();
        foreach ($this->columnMinWidths as $key => $width) {
            $dim = $this->list->getColumnDimension($key);
            if($dim->getWidth() < $width){
                $dim->setAutoSize(false);
                $dim->setWidth($width);
            }
        }
    }

    protected function getNameFromNumber($num) {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2) . $letter;
        } else {
            return $letter;
        }
    }
}
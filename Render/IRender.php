<?php

namespace TableOutput\Render;

use TableOutput\Row\ARow;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSummary;
use TableOutput\Table;

/**
 * Interface IRender
 */
interface IRender
{
    /**
     * @param Table $table
     * @param null $renderAsObject
     */
    public function __construct(Table $table, $renderAsObject = null);

    /**
     * Render table caption
     * @param $caption
     * @param $mergedCols
     * @return mixed
     */
    public function tableCaption($caption,$mergedCols);

    /**
     * Render header row
     * @param RowHeader $row
     * @return mixed
     */
    public function tableHeader(RowHeader $row);

    /**
     * Render row
     * @param ARow $row
     * @return mixed
     */
    public function tableRow(ARow $row);

    /**
     * Render summary row
     * @param RowSummary $row
     * @param bool $render
     * @return mixed
     */
    public function tableFooter(RowSummary $row = null, $render = true);

    /**
     * @return string
     */
    public function render();
}
<?php

namespace TableOutput\Row;

use TableOutput\Column\Column;
use TableOutput\Column\Header;
use TableOutput\Table;

/**
 * Class RowHeader
 */
class RowHeader extends ARow
{
    //default style
    public $background = true;
    public $fontSize = 11;
    public $bold = true;
    protected $countedWidth = 0;
    protected $countedItems = 0;

    /**
     * @param Table $table
     * @param array $row
     */
    public function __construct(Table $table,Array $row){
        $this->table = $table;
        if(empty($row)){
            $this->table->setCaption(t('REPORTING_NO_DATA'));
        }else{
            $this->countedWidth = 100 / count($row);
            $this->countedItems = count($row);
            foreach($row as $column => $value){
                $newColumn = $this->getDefaultColumn(new Header($this,$value),$this->countedWidth);
                $this->columns[$column] = $newColumn;
            }
        }
    }

    public function beforeRender(){
        if($this->countedItems != count($this->columns) && count($this->columns) > 0){
            $width = 100/count($this->columns);
            /** @var Column $column */
            foreach ($this->columns as $column) {
                $column->width = $width;
            }
        }
        parent::beforeRender();
    }

    public function setColumn($key,$column){
        if(array_key_exists($key,$this->columns)) $this->columns[$key] = $column;
    }

    public function deleteColumn($key){
        if(array_key_exists($key,$this->columns)) unset($this->columns[$key]);
    }

    public function sortHeaderByArrayKeys(Array $keys){
        $ordered = [];
        foreach ($keys as $key) {
            $ordered[$key] = $this->columns[$key];
        }
        $this->columns = $ordered;
    }
}
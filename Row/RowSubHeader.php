<?php

namespace TableOutput\Row;

use TableOutput\Column\Rowspan;
use TableOutput\Table;

/**
 * Class RowHeader
 */
class RowSubHeader extends RowHeader
{
    /**
     * @param Table $table
     * @param array $row
     * @param RowHeader $header
     */
    public function __construct(Table $table,Array $row, RowHeader $header){
        $this->table = $table;
        if(empty($row)){
            $this->table->setCaption(t('REPORTING_NO_DATA'));
        }else{
            $this->countedWidth = 100 / count($row);
            $this->countedItems = count($row);
            foreach($row as $column => $value){
                if(is_null($header->getColumn($column))) continue;
                $newColumn = $this->getDefaultColumn(new Rowspan($this,$value,$header->getColumn($column)),$this->countedWidth);
                $this->columns[$column] = $newColumn;
            }
        }
    }
}
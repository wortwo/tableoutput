<?php

namespace TableOutput\Row;

use TableOutput\Column\Colspan;
use TableOutput\Column\Column;
use TableOutput\Table;

/**
 * Class ARow
 */
abstract class ARow
{
    CONST COLSPAN = '@#$%^&*()_+';
    CONST SETTINGS_TO_ALL_COLUMNS = 'SETTINGS_TO_ALL_COLUMNS';

    /*** @var Table */
    protected $table;
    /*** @var Column[] */
    protected $columns = [];

    //default style
    public $background = false;
    public $border = true;
    public $fontSize = 10;
    public $bold = false;

    /**
     * @param Table $table
     * @param array $row
     */
    public function __construct(Table $table,Array $row){
        $this->table = $table;
        if(!empty($row)){
            $width = 100 / count($row);
            $overwtite = ($this instanceof RowSummary) ? []:$table->getDataOverwrite();
            /** @var Column $previousColumn */
            $previousColumn = NULL;
            foreach($row as $column => $value){
                if(array_key_exists($column,$overwtite)){
                    $value = $overwtite[$column];
                }
                if($value == self::COLSPAN && isset($previousColumn)){
                    $previousColumn->colspan++;
                    $newColumn = $this->getDefaultColumn(new Colspan($this,'',$previousColumn),$width);
                    $this->columns[$column] = $newColumn;
                    continue;
                }
                if(is_array($value)){
                    foreach ($value as $subColumn => $subValue) {
                        $newColumn = $this->getDefaultColumn(new Column($this,$subValue),$width/count($value));
                        $this->columns[$column.'_'.$subColumn] = $newColumn;
                        $previousColumn = $newColumn;
                        $this->table->addSubheader($column,$subColumn,$width/count($value));
                    }
                }else{
                    $newColumn = $this->getDefaultColumn(new Column($this,$value),$width);
                    $this->columns[$column] = $newColumn;
                    $previousColumn = $newColumn;
                }
            }
        }
    }

    public function addColumnAfter($column,$columnName,$afterColumn){
        if(array_key_exists($columnName,$this->columns)) return;
        $this->columns = $this->arrayInsertAfter($afterColumn,$this->columns,$columnName,$column);
    }

    /**
     * Inserts a new key/value after the key in the array.
     * @param mixed $key The key to insert after.
     * @param array $array An array to insert in to.
     * @param mixed $new_key The key to insert.
     * @param mixed $new_value An value to insert.
     * @return array|false
     * @see array_insert_before()
     */
    protected function arrayInsertAfter($key, array &$array, $new_key, $new_value) {
        if (array_key_exists($key, $array)) {
            $new = array();
            foreach ($array as $k => $value) {
                $new[$k] = $value;
                if ($k === $key) {
                    $new[$new_key] = $new_value;
                }
            }
            return $new;
        }
        return FALSE;
    }


    public function getColumn($name){
        return (is_array($this->columns) && array_key_exists($name,$this->columns)) ? $this->columns[$name]:null;
    }

    public function getColumnIndex($name){
        return array_search($name,array_keys($this->columns));
    }

    /**
     * Nastavi objektu bunky defaultni hodnoty a vrati jej
     * @param Column $column
     * @param float $width
     * @return Column
     */
    public function getDefaultColumn(Column $column,$width){
        $column->background = $this->background;
        $column->border = $this->border;
        $column->fontSize = $this->fontSize;
        $column->bold = $this->bold;
        $column->width = $width;
        return $column;
    }

    /**
     * Vrati pole bunek pro dany radek
     * @return Column[]
     */
    public function getColumns(){
        return $this->columns;
    }

    /**
     * Vrati pole vsech hodnot pro dany radek
     * @return array
     */
    public function toArray(){
        $result = [];
        foreach ($this->columns as $name => $column) {
            $result[$name] = $column->value;
        }
        return $result;
    }

    /**
     * Prida nastaveni bunek z hodnot v poli
     * @param string $setting
     * @param array $values
     * @return $this
     */
    public function setColumnSettings($setting,Array $values){
        foreach ($values as $column => $value) {
            if($column == self::SETTINGS_TO_ALL_COLUMNS) return $this->setColumnSettingToAllColumns($setting,$value);
            if(array_key_exists($column,$this->columns)) $this->columns[$column]->{$setting} = $value;
        }
        return $this;
    }

    protected function setColumnSettingToAllColumns($setting, $value){
        foreach($this->columns as $columnName => $column){
            $this->columns[$columnName]->{$setting} = $value;
        }
        return $this;
    }

    /**
     * @param $column
     * @return Column
     */
    public function __get($column){
        return $this->columns[$column];
    }

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function __set($column, $value){
        return $this->columns[$column]->value = $value;
    }

    /**
     * Pred vykreslenim se pripocita prodlouzenym bunkam sirka z prekrytych bunek
     * @return $this
     */
    public function beforeRender(){
        foreach ($this->columns as $column) {
            if($column instanceof Colspan) $column->column->width += $column->width;
        }
        return $this;
    }
}
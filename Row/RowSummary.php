<?php

namespace TableOutput\Row;

use TableOutput\Table;

/**
 * Class RowSummary
 *
 * !!! Pro pridani nove funkce je treba pridat CONST s nazvem funkce a methodu se jmenem zacinajicim "getProcessed" prijmajici pole hodnot ke zpracovani a vracejicim konecnou hodnotu
 * priklad: CONST SUMA = 'Suma'; protected function getProcessedSuma(Array $values){ ... return $result;}
 */
class RowSummary extends ARow
{
    CONST SUM = '1d623b89683f9ce4e074de1676d12416';
    CONST COUNT = 'e2942a04780e223b215eb8b663cf5353';

    protected $functions = [
        self::SUM => 'getProcessedSum',
        self::COUNT => 'getProcessedCount',
    ];

    //default style
    public $background = true;
    public $fontSize = 11;
    public $bold = true;

    protected $stopColspan = false;

    /**
     * @param Table $table
     * @param array $data
     * @param array $allColumns
     */
    public function __construct(Table $table, Array $data, Array $allColumns = []){
        $this->table = $table;
        //$refl = new ReflectionClass($this);
        //$constants = $refl->getConstants();
        if(!empty($data)){
            if(empty($allColumns)) parent::__construct($table,$data);
            $row = [];
            foreach ($allColumns as $column) {
                $row[$column] = NULL;
                //if(!$this->stopColspan) $row[$column] = ARow::COLSPAN;
                if(array_key_exists($column,$data)){
                    $row[$column] = $data[$column];
                }
                //if(!$this->stopColspan && in_array($row[$column],$constants)) $this->stopColspan = true;
            }
            parent::__construct($table,$row);
        }
    }

    /**
     * Pred vykreslenim se vypocitaji souctove hodnoty podle definovanych metod
     */
    public function beforeRender(){
        foreach ($this->columns as $column => $item) {
            if (!isset($this->functions[$item->value])){
                $item->outputFormat = 'default';
                continue;
            }
            $function = [$this,$this->functions[$item->value]];
            if(empty($item->value) || !is_callable($function)) continue;
            $this->{$column} = call_user_func($function,$this->table->getColumnValues($column));
        }
        parent::beforeRender();
    }

    /**
     * Vrati soucet vsech hodnot v poli
     * @param array $values
     * @return int
     */
    protected function getProcessedSum(Array $values){
        $result = 0;
        foreach ($values as $numeric) {
            if(!is_numeric($numeric)) continue;
            $result += $numeric;
        }
        return $result;
    }

    /**
     * Vrati pocet hodnot v poli
     * @param array $values
     * @return int
     */
    protected function getProcessedCount(Array $values){
        return count(array_filter($values));
    }
}

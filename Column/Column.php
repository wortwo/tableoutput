<?php

namespace TableOutput\Column;

use TableOutput\Lib\Helper;
use TableOutput\Row\ARow;

/**
 * Class Column
 */
class Column
{
    CONST ALIGN_LEFT = 'left';
    CONST ALIGN_RIGHT = 'right';
    CONST ALIGN_CENTER = 'center';

    CONST BORDER_ALL = true;
    CONST BORDER_NONE = false;
    CONST BORDER_TOP_BOTTOM = 'TB';
    CONST BORDER_LEFT_RIGHT = 'LR';

    CONST OUTPUT_DEFAULT = 'Default';
    CONST OUTPUT_DATE = 'Date';             //msgid "EXPORT_FORMAT_DATE" msgstr "dd.mm.yyyy"
    CONST OUTPUT_DATETIME = 'DateTime';     //msgid "EXPORT_FORMAT_DATETIME" msgstr "hh:mm dd.mm.yyyy"
    CONST OUTPUT_NUMBER = 'Number';    //msgid "EXPORT_FORMAT_NUMBER" msgstr "#\\ ###"
    CONST OUTPUT_PERCENT = 'Percent';    //msgid "EXPORT_FORMAT_PERCENT" msgstr "#\\ \\%"
    CONST OUTPUT_FLOAT_2 = 'Float2';    //msgid "EXPORT_FORMAT_FLOAT" msgstr "#\\ ##0.00"
    CONST OUTPUT_FLOAT_1 = 'Float1';    //msgid "EXPORT_FORMAT_FLOAT" msgstr "#\\ ##0.00"
    CONST OUTPUT_FLOAT_3 = 'Float3';    //msgid "EXPORT_FORMAT_FLOAT3" msgstr "#\\ ##0.000"
    CONST OUTPUT_FLOAT_6 = 'Float6';    //msgid "EXPORT_FORMAT_FLOAT3" msgstr "#\\ ##0.000"
    CONST OUTPUT_FLOAT_M2 = 'FloatM2';    //msgid "EXPORT_FORMAT_FLOAT_M2" msgstr "#\\ ##0.00\\ \\m\\2"
    CONST OUTPUT_FLOAT_M = 'FloatM';    //msgid "EXPORT_FORMAT_FLOAT_M" msgstr "#\\ ##0.00\\ \\m"
    CONST OUTPUT_MONEY = 'Money';    //msgid "EXPORT_FORMAT_FLOAT_KC" msgstr "#\\ ##0.00\\ \\Kč"
    CONST OUTPUT_MONTH_YEAR = 'MonthYear';  //msgid "EXPORT_FORMAT_DATE" msgstr "mm/yyyy"
    CONST OUTPUT_STR_LENGTH = 'StringLength';
    CONST OUTPUT_DOTTED_SIGNATURE = 'DottedSignature';
    CONST OUTPUT_FORMULA = 'Formula';

    protected $row;

    public $value;
    public $width;
    public $height = 5;
    public $border = false;
    public $align = null;
    public $background = false;
    public $colspan = 1;
    public $bold = false;
    public $fontSize = 10;
    public $outputFormat = self::OUTPUT_DEFAULT;
    public $dataType = null;

    /**
     * @param ARow $row
     * @param $value
     */
    public function __construct(ARow $row,$value){
        $this->row = $row;
        $this->value = $value;
    }

    public static function formulate($columnName){
        return '$['.$columnName.']$';
    }

    /**
     * Volani formatovaci funkce na hodnoty podle nastaveni objektu
     */
    public function beforeRender(){
        $this->applyOutputAlign();
        $this->applyOutputFormat();
    }

    //output align
    protected function applyOutputAlign(){
        if(isset($this->align)) return;
        $function = 'outputAlign'.$this->outputFormat;
        if(is_callable([$this,$function])){
            $this->{$function}();
        }else{
            $this->align = self::ALIGN_LEFT;
        }
    }

    protected function outputAlignMoney(){
        $this->align = self::ALIGN_RIGHT;
    }

    //output format
    public function applyOutputFormat(){
        $function = 'outputFormat'.$this->outputFormat;
        if(is_callable([$this,$function])) $this->{$function}();
    }

    protected function outputFormatMonthYear(){
        $this->value = date('m/Y',strtotime($this->value));
    }

    protected function outputFormatMoney(){
        $this->value = number_format($this->value, 0, ',', ' ').' Kč';
    }

    protected function outputFormatDate(){            //msgid "EXPORT_FORMAT_DATE" msgstr "dd.mm.yyyy"
        $this->value = date('d.m.Y',strtotime($this->value));
    }

    protected function outputFormatDateTime(){     //msgid "EXPORT_FORMAT_DATETIME" msgstr "hh:mm dd.mm.yyyy"
        $this->value = date('H:i d.m.Y',strtotime($this->value));
    }

    protected function outputFormatNumber(){    //msgid "EXPORT_FORMAT_NUMBER" msgstr "#\\ ###"
        $this->value = round($this->value);
    }

    protected function outputFormatPercent(){    //msgid "EXPORT_FORMAT_PERCENT" msgstr "#\\ \\%"
        $this->value = $this->value.' %';
    }

    protected function outputFormatFloat2(){    //msgid "EXPORT_FORMAT_FLOAT" msgstr "#\\ ##0.00"
        $this->value = round($this->value,2);
    }

    protected function outputFormatFloat1(){    //msgid "EXPORT_FORMAT_FLOAT" msgstr "#\\ ##0.00"
        $this->value = round($this->value,1);
    }

    protected function outputFormatFloat3(){    //msgid "EXPORT_FORMAT_FLOAT3" msgstr "#\\ ##0.000"
        $this->value = round($this->value,3);
    }

    protected function outputFormatFloatM2(){    //msgid "EXPORT_FORMAT_FLOAT_M2" msgstr "#\\ ##0.00\\ \\m\\2"
        $this->value = html_entity_decode(round($this->value,2).' m&#178;');
    }

    protected function outputFormatFloatM(){    //msgid "EXPORT_FORMAT_FLOAT_M" msgstr "#\\ ##0.00\\ \\m"
        $this->value = round($this->value,2).' m';
    }

    protected function outputFormatStringLength(){
        $this->value = Helper::mb_truncate($this->value,$this->width);
    }

    protected function outputFormatDottedSignature(){
        $this->value = ' '.str_repeat(".",$this->width-2).' ';
    }
}
<?php

namespace TableOutput\Column;

use TableOutput\Row\ARow;

/**
 * Class Rowspan
 */
class Rowspan extends Column
{
    /*** @var Column */
    public $column;

    /**
     * @param ARow $row
     * @param $value
     * @param Column $column
     */
    public function __construct(ARow $row,$value,Column $column){
        parent::__construct($row,$value);
        $this->column = $column;
    }
}
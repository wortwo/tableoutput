<?php

namespace TableOutput\Column;

/**
 * Class Header
 */
class Header extends Column
{
    /**
     * Zpracovani nazvu sloupce
     * TODO: lang
     */
    public function beforeRender(){
        $this->applyOutputAlign();
    }
}
<?php

namespace TableOutput;

use TableOutput\Column\Colspan;
use TableOutput\Column\Header;
use TableOutput\Render\IProgress;
use TableOutput\Render\IRender;
use TableOutput\Row\Row;
use TableOutput\Row\RowHeader;
use TableOutput\Row\RowSubHeader;
use TableOutput\Row\RowSummary;

/**
 * Class Table
 */
class Table
{
    protected $empty = false;
    protected $caption = '';
    /*** @var RowHeader */
    protected $header;
    /*** @var RowHeader */
    protected $subheader;
    /*** @var Row[] */
    protected $rows = [];
    protected $columnDefinitions = [];
    /*** @var RowSummary */
    protected $footer;

    protected $usedDataKeys = [];

    protected $settings = [];

    protected $renderAsObject;
    protected $renderSummaryRow = true;
    protected $dataOverwrite = [];
    protected $currentSubheaderKey = null;
    protected $autopageHandling = true;

    public function __construct($renderAsObject = null){
        $this->renderAsObject = $renderAsObject;
    }

    public function removeAutopageHandling(){
        $this->autopageHandling = false;
        return $this;
    }

    public function autohandlePages(){
        return $this->autopageHandling;
    }

    /**
     * Nastaveni nazvu tabulky
     * @param string $caption
     * @return $this
     */
    public function setCaption($caption){
        $this->caption = $caption;
        return $this;
    }

    public function removeHeader(){
        $this->header = null;
        return $this;
    }

    public function setDataOverwrite(Array $overwrite){
        $this->dataOverwrite = $overwrite;
        return $this;
    }

    public function getDataOverwrite(){
        return $this->dataOverwrite;
    }

    /**
     * Vlozeni pole dat tabulky
     * @param array $data
     * @return $this
     */
    public function addData(Array $data,$use = null){
        if(empty($data)) $this->empty = true;
        $this->addDataExclude($data, $use);
        return $this;
    }

    /**
     * Vlozeni pole dat tabulky ale pouze sloupcu definovanych v poli $use
     * @param array $data
     * @param array $use
     * @return $this
     */
    public function addDataExclude(Array $data, Array $use = null){
        $firstRow = ($this->empty) ? $data:array_shift($data);
        if(empty($use)) $use = array_keys($firstRow);
        $this->columnDefinitions = array_combine($use,$use);
        $headerRow = array_keys($firstRow);
        $headerRow = array_combine($headerRow,$headerRow);
        if(empty($this->header)) $this->header = new RowHeader($this,$this->getArrayIntersectKeySorted($headerRow,$this->columnDefinitions));
        $this->rows[] = new Row($this,$this->getArrayIntersectKeySorted($firstRow,$this->columnDefinitions));
        foreach($data as $row){
            $this->rows[] = new Row($this,$this->getArrayIntersectKeySorted($row,$this->columnDefinitions));
        }
        $this->usedDataKeys = $use;
        return $this;
    }

    /**
     * @param array $data
     * @param array $keys
     * @return array
     */
    protected function getArrayIntersectKeySorted(Array $data,Array $keys){
        $result = [];
        foreach ($keys as $key) {
            if(array_key_exists($key,$data)) $result[$key] = $data[$key];
        }
        return $result;
    }

    /**
     * Vrati pole hodnot podle nazvu sloupce
     * @param string $column
     * @return array
     */
    public function getColumnValues($column){
        $result = [];
        foreach($this->rows as $row){
            $result[] = $row->{$column}->value;
        }
        return $result;
    }

    /**
     * Prida nastaveni bunek z hodnot v poli
     * @param string $setting
     * @param array $values
     */
    protected function setColumnSettings($setting, Array $values){
        $this->settings[$setting] = $values;
    }

    /**
     * Aplikuj vsechna nastaveni bunek
     */
    protected function beforeRender(){
        if($this->empty) return;
        foreach ($this->settings as $setting => $values) {
            foreach ($this->rows as $row) {
                $row->setColumnSettings($setting,$values);
            }
            if(in_array($setting,['width','align','border','outputFormat'])){
                if(isset($this->header)) $this->header->setColumnSettings($setting,$values);
                if(isset($this->subheader)) $this->subheader->setColumnSettings($setting,$values);
                if(isset($this->footer)) $this->footer->setColumnSettings($setting,$values);
            }
        }
    }

    public function setAllColumnsSetting($setting,$value){
        $this->setColumnSettings($setting,array_fill_keys($this->usedDataKeys,$value));
        return $this;
    }

    /**
     * Nastavi procentualni sirky bunek vzhledem k sirce stranky z hodnot v poli
     * @param array $widths
     * @return $this
     */
    public function setColumnWidths(Array $widths){
        $this->setColumnSettings('width',$widths);
        return $this;
    }

    /**
     * Nastavi format hodnoty bunky z hodnot v poli
     * @param array $formats
     * @return $this
     */
    public function setColumnValueFormats(Array $formats){
        $this->setColumnSettings('outputFormat',$formats);
        return $this;
    }

    /**
     * Nastavi format ohraniceni bunky z hodnot v poli
     * @param array $formats
     * @return $this
     */
    public function setColumnBorderFormats(Array $formats){
        $this->setColumnSettings('border',$formats);
        return $this;
    }

    public function setColumnAlignFormats(Array $formats){
        $this->setColumnSettings('align',$formats);
        return $this;
    }

    public function setColumnBackgroundFormats(Array $formats){
        $this->setColumnSettings('background',$formats);
        return $this;
    }

    public function setColumnBoldFormats(Array $formats){
        $this->setColumnSettings('bold',$formats);
        return $this;
    }

    public function setColumnFontSizeFormats(Array $formats){
        $this->setColumnSettings('fontSize',$formats);
        return $this;
    }

    /**
     * Nastavi preklady nazvu sloupcu
     * @param array $headerLangs
     * @return $this
     */
    public function setHeaderLangs(Array $headerLangs){
        if($this->empty || empty($this->header)) return $this;
        foreach ($this->header->getColumns() as $column => $header) {
            if(array_key_exists($column,$headerLangs)) $header->value = $headerLangs[$column];
        }
        return $this;
    }

    /**
     * Nastavi posleni souctovy rader
     * @param array $data
     */
    public function setSummaryRow(Array $data){
        if($this->empty) return;
        $this->footer = new RowSummary($this,$data,array_keys($this->header->toArray()));
    }

    /**
     * Vrati posledni souctovy radek
     * @return RowSummary
     */
    public function getSummaryRow(){
        return $this->footer;
    }

    /**
     * Vrati prvni hlavickovy radek
     * @return RowHeader
     */
    public function getHeaderRow(){
        return $this->header;
    }

    public function setRenderSummaryRow($render = true){
        $this->renderSummaryRow = $render;
    }

    /**
     * Vrati vykreslenou tabulku
     * @param string $type
     * @param IProgress $callProgress
     * @return string
     * @throws \Exception
     */
    public function render($type,IProgress $callProgress = null){
        $this->beforeRender();
        $renderer = $this->getRendererByType($type);
        $renderer->tableCaption($this->caption,count($this->columnDefinitions));
        if($this->header instanceof RowHeader) $renderer->tableHeader($this->header);
        if($this->subheader instanceof RowHeader) $renderer->tableHeader($this->subheader);
        foreach ($this->rows as $key => $row) {
            $renderer->tableRow($row);
            if(is_callable([$callProgress,'addStep'])) $callProgress->addStep();
        }
        if($this->footer instanceof RowSummary) $renderer->tableFooter($this->footer, $this->renderSummaryRow);
        return $renderer->render();
    }

    public function addSubheader($column,$subColumn,$width){
        if(!$this->header instanceof RowHeader) return;
        if(empty($this->subheader)){
            $this->subheader = new RowSubHeader($this,$this->columnDefinitions,$this->header);
        }
        $columnName = $column.'_'.$subColumn;
        $subHeader = $this->subheader->getDefaultColumn(new Header($this->subheader,$subColumn),$width);
        $this->subheader->addColumnAfter($subHeader,$columnName,$column);
        if($this->currentSubheaderKey == $column){
            $colspan = new Colspan($this->header,$column,$this->header->getColumn($column));
            $this->header->addColumnAfter($colspan,$columnName,$column);
        }
        $this->subheader->getColumn($column)->value = null;
        $this->currentSubheaderKey = $column;
    }

    public function setSubHeaderOrder($subHeaderName,Array $order){
        $unordered = $this->subheader->getColumns();
        $orderedPart = [];
        foreach ($order as $value){
            $key = $subHeaderName.'_'.$value;
            if(array_key_exists($key,$unordered)) $orderedPart[$key] = $key;
        }
        $ordered = [];
        $shiftingOrderedPart = $orderedPart;
        foreach ($this->subheader->getColumns() as $key => $column) {
            if(!array_key_exists($key,$orderedPart)){
                $ordered[] = $key;
            }else{
                $ordered[] = array_shift($shiftingOrderedPart);
            }
        }
        $this->subheader->sortHeaderByArrayKeys($ordered);
    }

    /**
     * Vrati instanci tridy pro vykresleni podle typu
     * @param string $type
     * @return IRender
     * @throws \Exception
     */
    protected function getRendererByType($type){
        $class = 'TableOutput\Render\Render'.$type;
//        $directory = __DIR__.'/Render';
//        foreach (array_diff(scandir($directory), Array('.', '..', 'IRender.php','IProgress.php')) as $file) {
//            if($file == $class.'.php'){
//                include_once $directory.'/'.$file;
//                $ref = new \ReflectionClass(ucfirst($class));
//                if($ref->isInstantiable() && $ref->implementsInterface('IRender')) return $ref->newInstance($this,$this->renderAsObject);
//            }
//        }
        if(class_exists($class)){
            return new $class($this,$this->renderAsObject);
        }
        throw new \Exception('Unknown render type ('.$type.').');
    }

    /**
     * @return array
     */
    public function getAvailableRenderTypes()
    {
        $result = [];
        $directory = __DIR__.'/Render';
        foreach (array_diff(scandir($directory), Array('.', '..', 'IRender.php','IProgress.php')) as $file) {
            $type = str_replace(['Render','.php'],'',$file);
            $class = 'TableOutput\Render\Render'.$type;
            try{
                //if(!class_exists($class)) continue;
                $ref = new \ReflectionClass($class);
                if($ref->isInstantiable() && $ref->implementsInterface('TableOutput\Render\IRender')){
                    $result[] = $type;
                }
            }catch (\Exception $e){
                continue;
            }
        }
        return $result;
    }
}

